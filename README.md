# PowerCollections
Welcome to Power Collections, A Community Project to Develop the Best Public License type safe Collection Classes for .NET. Power Collections makes heavy use of .NET Generics. The goal of the project is to provide generic collection classes that are not available in the .NET framework. Some of the collections included are the Deque, MultiDictionary, Bag, OrderedBag, OrderedDictionary, Set, OrderedSet, and OrderedMultiDictionary.

This library was originally produced by Wintellect and is offered AS IS.
It has been available on the Wintellect site for some time was later placed on Codeplex until Codeplex was shut down.

https://archive.codeplex.com/?p=powercollections

Changes made to the origional Project:

* Adapted for .NET Standard 2.1
* Added CultureInfo in Algorithms ToString functions to pass all UnitTests